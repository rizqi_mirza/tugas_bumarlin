<!DOCTYPE html>
<html lang="en">
<?php
session_start();
?>
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Connect Plus</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="back-end/assets/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="back-end/assets/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="back-end/assets/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="back-end/assets/css/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="back-end/assets/images/favicon.png" />
  </head>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-5">
                <?php 
                if(isset($_GET['pesan'])){
                    if($_GET['pesan']=="gagal"){
                        echo "<div class='alert'>Username dan Password tidak sesuai !</div>";
                    }
                }
	            ?>
                <div class="brand-logo">
                  <img src="back-end/assets/images/logo-dark.svg">
                </div>
                <h4>Hello! let's get started</h4>
                <h6 class="font-weight-light">Sign in to continue.</h6>
                <form class="pt-3" action="dashboard/config/cekLogin.php" method="GET">
                  <div class="form-group">
                    <input type="text" class="form-control form-control-lg" placeholder="Username" name="username" required>
                  </div>
                  <div class="form-group">
                    <input type="password" class="form-control form-control-lg"  placeholder="Password" name="password" required>
                  </div>
                  <div class="mt-3">
                    <button class="btn btn-block btn-success btn-lg font-weight-medium auth-form-btn">SIGN IN</button>
                  </div>
                  <!-- <div class="my-2 d-flex justify-content-between align-items-center">
                    <div class="form-check">
                      <label class="form-check-label text-muted">
                        <input type="checkbox" class="form-check-input"> Keep me signed in </label>
                    </div>
                    <a href="#" class="auth-link text-black">Forgot password?</a>
                  </div> -->
                  <!-- <div class="mb-2">
                    <button type="button" class="btn btn-block btn-facebook auth-form-btn">
                      <i class="mdi mdi-facebook mr-2"></i>Connect using facebook </button>
                  </div> -->
                  <div class="text-center mt-4 font-weight-light"> Don't have an account? <a href="registrasi.php" class="text-primary">Create</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="back-end/assets/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="back-end/assets/js/off-canvas.js"></script>
    <script src="back-end/assets/js/hoverable-collapse.js"></script>
    <script src="back-end/assets/js/misc.js"></script>
    <!-- endinject -->
  </body>
</html>