<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Connect Plus</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="../../back-end/assets/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="../../back-end/assets/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="../../back-end/assets/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="../../back-end/assets/css/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="../../back-end/assets/images/favicon.png" />
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:../../partials/_navbar.html -->
      <?php include 'navbar.php';?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
       <?php include 'sidebar.php';?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> Basic Tables </h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Tables</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Basic tables</li>
                </ol>
              </nav>
            </div>
            <div class="row">
              <div class="col-lg-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                  <?php
                        $lahan = $_POST['lahan'];
                    ?>
                    <h4 class="card-title">Input Kadar Pupuk dengan Luas Lahan <?php echo $lahan; ?></h4>
                    <!-- <p class="card-description"> Add class <code>.table-bordered</code> -->
                    </p>
                    <form action="hasil-eliminasi.php" method="POST">
                    <input type="hidden" name="luas_lahan" value="<?php echo $lahan; ?>"  >
                        <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th> # </th>
                            <th> CMK (X) </th>
                            <th> CRM (Y) </th>
                            <th> CRH (Z) </th>
                            <th> Jumlah Pupuk </th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td> NPK</td>
                            <td><input type="text" name="npk-x" class="form-control"></td>
                            <td><input type="text" name="npk-y" class="form-control"></td>
                            <td><input type="text" name="npk-z" class="form-control"></td>
                            <td><input type="text" name="npk-jumlah" class="form-control"></td>
                            </tr>
                            <tr>
                            <td> TSP </td>
                            <td><input type="text" name="tsp-x" class="form-control"></td>
                            <td><input type="text" name="tsp-y" class="form-control"></td>
                            <td><input type="text" name="tsp-z" class="form-control"></td>
                            <td><input type="text" name="tsp-jumlah" class="form-control"></td>
                            </tr>
                            <tr>
                            <td> KCL </td>
                            <td><input type="text" name="kcl-x" class="form-control"></td>
                            <td><input type="text" name="kcl-y" class="form-control"></td>
                            <td><input type="text" name="kcl-z" class="form-control"></td>
                            <td><input type="text" name="kcl-jumlah" class="form-control"></td>
                          </tr>
                            
                        </tbody>
                        </table>
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                            </div>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <footer class="footer">
            <div class="footer-inner-wraper">
              <div class="d-sm-flex justify-content-center justify-content-sm-between">
                <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © bootstrapdash.com 2020</span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> Free <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap dashboard templates</a> from Bootstrapdash.com</span>
              </div>
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="../../back-end/assets/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="../../back-end/assets/js/off-canvas.js"></script>
    <script src="../../back-end/assets/js/hoverable-collapse.js"></script>
    <script src="../../back-end/assets/js/misc.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <!-- End custom js for this page -->
  </body>
</html>