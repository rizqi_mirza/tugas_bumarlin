<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Connect Plus</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="../../back-end/assets/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="../../back-end/assets/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="../../back-end/assets/vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="../../back-end/assets/vendors/select2/select2.min.css">
    <link rel="stylesheet" href="../../back-end/assets/vendors/select2-bootstrap-theme/select2-bootstrap.min.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="../../back-end/assets/css/style.css">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="../../back-end/assets/images/favicon.png" />
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:../../partials/_navbar.html -->
      <?php include 'navbar.php';?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
        <?php include 'sidebar.php';?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="page-header">
              <h3 class="page-title"> Form elements </h3>
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Forms</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Form elements</li>
                </ol>
              </nav>
            </div>
            <div class="row">
              <div class="col-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Langkah 1</h4>
                    <p class="card-description"> Menginputkan Lahan Tersedia </p>
                    <form class="forms-sample" action="create-kadarpupuk.php" method="POST">
                      <div class="form-group">
                        <label for="exampleSelectGender">Input Lahan</label>
                        <select class="form-control" id="exampleSelectGender" name="lahan">
                            <option value="">--Pilih Lebar Lahan--</option>
                            <option value="1000-1999">1000-1999</option>
                            <option value="2000-2999">2000-2999</option>
                            <option value="3000-3999">3000-3999</option>
                        </select>
                      </div>
                      <button type="submit" class="btn btn-success mr-2">Submit</button>
                      <!-- <button class="btn btn-light">Cancel</button> -->
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
          <!-- partial:../../partials/_footer.html -->
          <footer class="footer">
            <div class="footer-inner-wraper">
              <div class="d-sm-flex justify-content-center justify-content-sm-between">
                <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © bootstrapdash.com 2020</span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> Free <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap dashboard templates</a> from Bootstrapdash.com</span>
              </div>
            </div>
          </footer>
          <!-- partial -->
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="../../back-end/assets/vendors/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="../../back-end/assets/vendors/select2/select2.min.js"></script>
    <script src="../../back-end/assets/vendors/typeahead.js/typeahead.bundle.min.js"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="../../back-end/assets/js/off-canvas.js"></script>
    <script src="../../back-end/assets/js/hoverable-collapse.js"></script>
    <script src="../../back-end/assets/js/misc.js"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="../../back-end/assets/js/file-upload.js"></script>
    <script src="../../back-end/assets/js/typeahead.js"></script>
    <script src="../../back-end/assets/js/select2.js"></script>
    <!-- End custom js for this page -->
  </body>
</html>